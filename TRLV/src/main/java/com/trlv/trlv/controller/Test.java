package com.trlv.trlv.controller;

import java.sql.*;

public class Test {
    static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    static final String USER = "postgres";
    static final String PASS = "vanthai1";
    static final String QUERY = "select l.id, l.title_letter , c.name as categories_letter, l.image_letter , l.link , l.content_letter " +
            "from letter l, letter_categories lc , categories c " +
            "where  " +
            "l.link = 'letter/letter2'" +
            "and l.id  = lc.letter_id and lc.categories_id = c.id";
    public static void main(String[] args) {
        // Open a connection
        System.out.println("Start");
        try(Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(QUERY);
        ) {
            while(rs.next()){
                //Display values
                System.out.print("ID: " + rs.getInt("id"));
//                System.out.print(", Age: " + rs.getInt("age"));
                System.out.println(", Name: " + rs.getString("categories_letter"));
//                System.out.println(", Last: " + rs.getString("last"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
