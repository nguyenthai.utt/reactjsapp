package com.trlv.trlv.controller;

import com.trlv.trlv.dto.LetterCategoriesDTO;
import com.trlv.trlv.response.LetterByCategories;
import com.trlv.trlv.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoriesController {

    @Autowired
    CategoriesService categoriesService;

    @GetMapping("/categories")
    @CrossOrigin(origins = "*")
    public LetterByCategories getLetterByCategories(@Param("id") Integer id) {
        return categoriesService.getListLetterByCategoriesId(id);
    }

}
