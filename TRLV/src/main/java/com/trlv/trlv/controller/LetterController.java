package com.trlv.trlv.controller;

import com.trlv.trlv.dto.LetterResponseDTO;
import com.trlv.trlv.response.LetterResponse;
import com.trlv.trlv.service.LetterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LetterController {

    @Autowired
    private LetterService letterService;

    @GetMapping("/letterId")
    @CrossOrigin(origins = "*")
    public LetterResponse getLetterById(@Param("id")Long id) {
        return letterService.getLetterById(id);
    }
}
