package com.trlv.trlv.response;

import com.trlv.trlv.dto.CategoriesDTO;
import com.trlv.trlv.dto.LetterDTO;
import com.trlv.trlv.dto.LetterResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
public class LetterResponse implements Serializable {
    private LetterResponseDTO letter;
    private List<LetterByCategories> listcategories;

    public LetterResponse(LetterResponseDTO letter, List<LetterByCategories> listcategories) {
        this.letter = letter;
        this.listcategories = listcategories;
    }

//    public LetterResponse(LetterResponseDTO letterResponseDTO, List<CategoriesDTO> categoriesDTOList) {
//        this.letter = letterResponseDTO;
//        for (CategoriesDTO t : this.listcategories = categoriesDTOList) {
//
//        }
//
//    }
}
