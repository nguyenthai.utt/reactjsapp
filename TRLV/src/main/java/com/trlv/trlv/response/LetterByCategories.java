package com.trlv.trlv.response;

import com.trlv.trlv.dto.CategoriesDTO;
import com.trlv.trlv.dto.LetterCategoriesDTO;
import com.trlv.trlv.dto.LetterDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@AllArgsConstructor
public class LetterByCategories implements Serializable {
    private List<LetterDTO> data;

    private Optional<CategoriesDTO> categories;

    public LetterByCategories(List<LetterDTO> dataById) {
        this.data = dataById;
    }

    public LetterByCategories() {

    }

    public void setCategories(Optional<CategoriesDTO> categoriesDTO) {
        this.categories = categoriesDTO;
    }
}
