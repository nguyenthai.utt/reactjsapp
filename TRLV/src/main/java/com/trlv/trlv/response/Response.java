package com.trlv.trlv.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Response<T> {
    private List<T> data;

    public Response() {

    }

    public Response responseData() {
        return new Response(this.data);
    }
}
