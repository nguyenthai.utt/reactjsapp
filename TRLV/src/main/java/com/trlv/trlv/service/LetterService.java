package com.trlv.trlv.service;

import com.trlv.trlv.dto.CategoriesDTO;
import com.trlv.trlv.dto.LetterCategoriesDTO;
import com.trlv.trlv.dto.LetterDTO;
import com.trlv.trlv.dto.LetterResponseDTO;
import com.trlv.trlv.entity.EntityValues;
import com.trlv.trlv.repository.CategoriesRepository;
import com.trlv.trlv.repository.LetterCategoriesRepository;
import com.trlv.trlv.repository.LetterRepository;
import com.trlv.trlv.response.LetterByCategories;
import com.trlv.trlv.response.LetterResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LetterService {
    @Autowired
    LetterRepository letterRepository;

    @Autowired
    LetterCategoriesRepository letterCategoriesRepository;

    @Autowired
    CategoriesRepository categoriesRepository;

    public LetterResponse getLetterById(Long id) {

        Optional<LetterDTO> letterDTO = Optional.of(new LetterDTO(letterRepository.findById(id)));
//        letterDTO = letterRepository.findById(id);
//        List<LetterCategoriesDTO> letterCategoriesDTOList = new ArrayList<LetterCategoriesDTO>(letterCategoriesRepository.getDataById(Math.toIntExact(id)));
        List<LetterCategoriesDTO> letterCategoriesDTOList = new ArrayList<LetterCategoriesDTO>(letterCategoriesRepository.getDataByLetterId(id));

//        LetterResponseDTO letterResponseDTO = new LetterResponseDTO(letterDTO,categoriesDTOList);
//
        List<LetterByCategories> letterByCategoriesList = new ArrayList<>();
        EntityValues.List_TITLE_CATEGORIES.forEach(item ->{
            LetterByCategories letterByCategories = new LetterByCategories();
            letterByCategories.setData(letterRepository.findDataLetterCategories(item.getId(), EntityValues.LIMIT_LETTER_HOME));
            letterByCategories.setCategories(categoriesRepository.findById(item.getId()));
            letterByCategoriesList.add(letterByCategories);
        });
        return new LetterResponse(new LetterResponseDTO(letterDTO,letterCategoriesDTOList),letterByCategoriesList);
    }


}
