package com.trlv.trlv.service;

import com.trlv.trlv.dto.CategoriesDTO;
import com.trlv.trlv.dto.LetterDTO;
import com.trlv.trlv.dto.MenuDTO;
import com.trlv.trlv.entity.EntityValues;
import com.trlv.trlv.repository.CategoriesRepository;
import com.trlv.trlv.repository.HomeRepository;
import com.trlv.trlv.repository.LetterRepository;
import com.trlv.trlv.response.LetterByCategories;
import com.trlv.trlv.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class HomeService {

    @Autowired
    private HomeRepository homeRepository;

    @Autowired
    private LetterRepository letterRepository;

    @Autowired
    CategoriesRepository categoriesRepository;

    public List<MenuDTO> getDataMenu() {
        return homeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public List<LetterByCategories> getDataLetter() {
        List<LetterByCategories> letterByCategoriesList = new ArrayList<>();
        EntityValues.List_TITLE_CATEGORIES.forEach(item ->{
            LetterByCategories letterByCategories = new LetterByCategories();
            letterByCategories.setData(letterRepository.findDataLetterCategories(item.getId(), EntityValues.LIMIT_LETTER_HOME));
            Optional<CategoriesDTO> categoriesDTO = Optional.of(new CategoriesDTO());
            categoriesDTO = categoriesRepository.findById(item.getId());
            letterByCategories.setCategories(categoriesDTO);
            letterByCategoriesList.add(letterByCategories);
        });

        return letterByCategoriesList;
    }

//    public List<LetterResponseDTO> getDataLetter() {
//
//        List<LetterDTO> allLetters = letterRepository.findAllDataLetter();
//
//        Map<Long, List<LetterDTO>> groupedLetters = new HashMap<>();
//        allLetters.forEach(letterDTO -> {
//            if (groupedLetters.containsKey(letterDTO.getId())) {
//                groupedLetters.get(letterDTO.getId()).add(letterDTO);
//            } else {
//                groupedLetters.put(letterDTO.getId(), new ArrayList<>(Collections.singletonList(letterDTO)));
//            }
//        });
//
//        List<LetterResponseDTO> response = new ArrayList<>();
//        groupedLetters.forEach((id, letters) -> {
//            response.add(LetterResponseDTO.createFromLetter(letters));
//        });
//        return response;
//    }

//    public LetterResponseDTO getpost(String link) {
//        return LetterResponseDTO.createFromLetter(letterRepository.findLetterById(link));
//    }

//    public List<LetterDTO> getpost(Long id) {
//        return letterRepository.findLetterById(id);
//    }

//    public List<LetterDTO> getDataCategories(Long id, Long limit) {
//        return letterRepository.findDataCategoriesById(id, limit);
//    }


}
