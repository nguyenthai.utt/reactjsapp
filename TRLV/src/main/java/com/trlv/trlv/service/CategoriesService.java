package com.trlv.trlv.service;

import com.trlv.trlv.dto.LetterCategoriesDTO;
import com.trlv.trlv.entity.EntityValues;
import com.trlv.trlv.repository.CategoriesRepository;
import com.trlv.trlv.repository.LetterRepository;
import com.trlv.trlv.response.LetterByCategories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoriesService {

    @Autowired
    LetterRepository letterRepository;

    @Autowired
    CategoriesRepository categoriesRepository;

    public LetterByCategories getListLetterByCategoriesId(Integer id) {
        LetterByCategories letterByCategories = new LetterByCategories(letterRepository.findDataLetterCategories(id, EntityValues.LIMIT_LETTER_CATEGORIES));
        letterByCategories.setCategories(categoriesRepository.findById(id));
        return letterByCategories;
    }
}
