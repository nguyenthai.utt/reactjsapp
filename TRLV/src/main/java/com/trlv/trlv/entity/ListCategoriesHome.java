package com.trlv.trlv.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ListCategoriesHome {
    private int id;
    private String title;

    public ListCategoriesHome() {
    }

    public static List<ListCategoriesHome> getListSetUp() {

        List<ListCategoriesHome> response = new ArrayList<>();

        response.add(new ListCategoriesHome(0,"Check out these EPIC Destinations!"));
        response.add(new ListCategoriesHome(4,"Categories 2"));

        return response;
    }

    public static List<ListCategoriesHome> getListForLetterSetUp() {

        List<ListCategoriesHome> response = new ArrayList<>();

        response.add(new ListCategoriesHome(0,"News Letter"));
        response.add(new ListCategoriesHome(4,"Categories 2"));

        return response;
    }
}
