package com.trlv.trlv.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EntityValues {
    public static final int LIMIT_LETTER_HOME = 20;
    public static final int LIMIT_LETTER_CATEGORIES = 20;
    public static final List<ListCategoriesHome> List_TITLE_CATEGORIES = ListCategoriesHome.getListSetUp();
    public static final List<ListCategoriesHome> LIST_ID_CATEGORIES_FOR_LETTER = ListCategoriesHome.getListForLetterSetUp();
}
