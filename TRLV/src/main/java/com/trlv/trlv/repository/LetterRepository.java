package com.trlv.trlv.repository;

import com.sun.istack.NotNull;
import com.trlv.trlv.dto.LetterDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LetterRepository extends JpaRepository<LetterDTO, Long> {

//
//    @Autowired
//    ConnectSever connectSever = new ConnectSever();

    @Query(value = "select * from letter limit :limit", nativeQuery = true)
    List<LetterDTO> findDataLetterLimit(@Param("limit") int limitLetterHome);

    @Query(value = "select l.id, l.title_letter, l.image_letter, l.link, l.content_letter from letter_categories lc join letter l " +
            "on lc.letter_id = l.id where lc.categories_id = :id limit :limit", nativeQuery = true)
    List<LetterDTO> findDataLetterCategories(@Param("id") int id , @Param("limit") int limitLetterHome);

    @Query(value = "select l.id, l.title_letter, l.image_letter, l.link from letter_categories lc " +
            "join letter l on lc.letter_id = l.id where lc.categories_id = :id limit :limit", nativeQuery = true)
    List<LetterDTO> getLetterByCategories(@Param("id") Long id, @Param("limit") int limitLetterHome);
}
