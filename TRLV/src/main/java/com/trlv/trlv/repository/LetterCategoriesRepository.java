package com.trlv.trlv.repository;

import com.trlv.trlv.dto.CategoriesDTO;
import com.trlv.trlv.dto.LetterCategoriesDTO;
import com.trlv.trlv.entity.ConnectSever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public interface LetterCategoriesRepository extends JpaRepository<LetterCategoriesDTO, Integer> {
    @Query(value = "select lc.id, lc.letter_id, c.name as categories_id, c.link from letter_categories lc join categories c " +
            "on lc.categories_id = c.id where lc.letter_id = :id",nativeQuery = true)
    List<LetterCategoriesDTO> getDataByLetterId(@Param("id") Long id);


        @Autowired
        ConnectSever connectSever = new ConnectSever();

    public default List<LetterCategoriesDTO> getDataById(int id) {
        String sql = "select lc.letter_id, c.name as categories_id from letter_categories lc join categories c on lc.categories_id = c.id where lc.letter_id = ?";
        List<LetterCategoriesDTO> response = null;
        try {
            response = new ArrayList<>();
            PreparedStatement stmt = connectSever.getConn().prepareStatement(sql);
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();

            response = new ArrayList<>();
            while (rs.next()) {

                //Creat new Letter
                LetterCategoriesDTO letterCategoriesDTO = new LetterCategoriesDTO();

                //ID values
                letterCategoriesDTO.setLetter_id(rs.getInt("letter_id"));

                //Title Value
                letterCategoriesDTO.setCategories_id(rs.getString("categories_id"));

                response.add(letterCategoriesDTO);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return response;
    };
}
