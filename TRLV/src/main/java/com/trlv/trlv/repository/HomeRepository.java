package com.trlv.trlv.repository;

import com.trlv.trlv.dto.LetterDTO;
import com.trlv.trlv.dto.MenuDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HomeRepository extends JpaRepository<MenuDTO,Long> {

}
