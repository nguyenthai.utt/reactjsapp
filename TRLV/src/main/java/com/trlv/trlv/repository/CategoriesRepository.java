package com.trlv.trlv.repository;

import com.trlv.trlv.dto.CategoriesDTO;
import com.trlv.trlv.dto.LetterCategoriesDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoriesRepository extends JpaRepository<CategoriesDTO, Integer> {

}
