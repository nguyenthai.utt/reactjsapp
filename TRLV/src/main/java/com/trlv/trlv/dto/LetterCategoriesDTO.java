package com.trlv.trlv.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "letter_categories")
@AllArgsConstructor
public class LetterCategoriesDTO implements Serializable {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "letter_id")
    private Integer letter_id;

    @Column(name = "categories_id")
    private String categories_id;

    @Column(name = "link")
    private String link;

    public LetterCategoriesDTO() {
    }

    public LetterCategoriesDTO(List<LetterCategoriesDTO> dataById) {
    }
}
