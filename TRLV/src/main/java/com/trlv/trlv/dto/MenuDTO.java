package com.trlv.trlv.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(name = "menutrlv")
public class MenuDTO implements Serializable {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "name_item")
    private String name;

    @Column(name = "link_item")
    private String link;

    public MenuDTO() {
    }
}
