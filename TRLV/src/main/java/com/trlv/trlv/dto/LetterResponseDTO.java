package com.trlv.trlv.dto;

import com.trlv.trlv.response.LetterByCategories;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
public class LetterResponseDTO implements Serializable {

    private Long id;

    private String title;

    private List<LetterCategoriesDTO> categories;

    private String image;

    private String link;

    private String content;

    public LetterResponseDTO(Optional<LetterDTO> letterDTO, List<LetterCategoriesDTO> letterCategoriesDTOList) {
        this.id = letterDTO.get().getId();
        this.title = letterDTO.get().getTitle();
        this.categories = letterCategoriesDTOList;
        this.image = letterDTO.get().getImage();
        this.link = letterDTO.get().getLink();
        this.content = letterDTO.get().getContent();
    }

//    public static LetterResponseDTO createFromLetter(List<LetterDTO> letters) {
//        LetterDTO template = letters.get(0);
//        List<String> categories = letters.stream().map(LetterDTO::getCategories).collect(Collectors.toList());
//        return new LetterResponseDTO(template.getId(), template.getTitle(), categories, template.getImage(), template.getLink(), template.getContent());
//    }
    public static List<String> getDataCategories(List<LetterCategoriesDTO> letterCategoriesDTOList) {
        List<String> response = new ArrayList<>();
        letterCategoriesDTOList.forEach(item->{
            response.add(item.getCategories_id());
        });
        return response;
    }
}
