package com.trlv.trlv.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.Singular;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Optional;

@Entity(name = "letter")
@Getter
@Setter
@AllArgsConstructor
public class LetterDTO implements Serializable {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title_letter")
    private String title;

    @Column(name = "image_letter")
    private String image;

    @Column(name = "link")
    private String link;

    @Column(name = "content_letter")
    private String content;

    public LetterDTO(Optional<LetterDTO> byId) {
        this.id = byId.get().getId();
        this.title = byId.get().getTitle();
        this.image = byId.get().getImage();
        this.link = byId.get().getLink();
        this.content = byId.get().getContent();
    }

    public LetterDTO() {
    }

    public LetterDTO(Long id) {
        this.id = id;
    }

    public LetterDTO(int i, String explore_the, String exploreThe, String s, String s1) {
    }
}
